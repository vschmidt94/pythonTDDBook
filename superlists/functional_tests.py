"""Code from TDD with Python book
functional tests as following along the book:
Test Driven Development with Python
written by Harry Percival

This is just me following along with the book and maybe some deviations
along the way.
"""
import unittest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.firefox.firefox_binary import FirefoxBinary

class NewVisitorTest(unittest.TestCase):
    """Functional Tests from standpoint of new visitor to our site.
    """

    def setUp(self):
        """Initializes the tests
        """
        self.browser = webdriver.Firefox(firefox_binary=FirefoxBinary(
            firefox_path='/home/vschmidt/firefox-esr/firefox'
        ))

        # Don't rely on implicitly_wait() beyond elementary web pages
        self.browser.implicitly_wait(3)

    def tearDown(self):
        """Cleans up after the tests
        """
        self.browser.quit()

    def test_can_start_a_list_and_retrieve_it_later(self):
        """User can start a new to-do list and retrieve it later

        Percival's naming conventions don't follow PEP-8, but he encourages
        the use of descriptive and readable test names.
        """

        # H. Percival likes a narrative / user story to help explain code
        # Understandable on one hand, but quite burdensome. I've taken some
        # libertiees here.

        # User goes to app homepage
        self.browser.get('http://localhost:8000')

        # User notices the page title and header mention to-do lists.
        self.assertIn('To-Do', self.browser.title)
        header_text = self.browser.find_element_by_tag_name('h1').text
        self.assertIn('To-Do', header_text)

        # User enters a to-do item straight away
        inputbox = self.browser.find_element_by_id('id_new_item')
        self.assertEqual(
            inputbox.get_attribute('placeholder'),
            'Enter a to-do item'
        )

        # User enters to-do into text box.
        inputbox.send_keys('Make a plumbus')

        # When user hits enter, the page updates and not the page lists
        # '1: Make a plumbus' as an item in a to-do list table
        inputbox.send_keys(Keys.ENTER)

        table = self.browser.find_element_by_id('id_list_table')
        rows = table.find_elements_by_tag_name('tr')
        self.assertTrue(
            any(row.text == '1: Make a plumbus' for row in rows),
            "New to-do item did not appear in table"
        )

        # This is a hard fail to remind me to finish the test later.
        self.fail("The test is incomplete, needs to be finished!")

if __name__ == '__main__':
    unittest.main()

